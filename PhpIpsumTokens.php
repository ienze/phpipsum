<?php

namespace Ienze\PhpIpsum;

interface IpsumToken {

	public function write(PhpIpsum $generator);
}

class IpsumBoolean implements IpsumToken {

	public function write(PhpIpsum $generator) {
		if (rand(0, 1) == 1) {
			return 'TRUE';
		} else {
			return 'FALSE';
		}
	}

}

class IpsumInteger implements IpsumToken {

	public function write(PhpIpsum $generator) {
		$i = 1;
		while (rand(0, 4) <= 3) {
			$i++;
		}
		return $i;
	}

}

class IpsumFloat implements IpsumToken {

	public function write(PhpIpsum $generator) {
		$i = 0.0;
		while (rand(0, 4) <= 3) {
			$i += mt_rand() / mt_getrandmax();
		}
		return number_format($i, rand(0, 6));
	}

}

class IpsumConcat implements IpsumToken {

	public function write(PhpIpsum $generator) {
		$gtkns = ['string', 'get_variable', 'concat', 'integer', 'boolean'];
		return $generator->token($gtkns) . ' . ' . $generator->token($gtkns);
	}

}

class IpsumSetVariable implements IpsumToken {

	public static $usedVars = [];

	public function write(PhpIpsum $generator) {
		$var = str_replace(" ", "_", PhpIpsum::randomString());
		
		$r = '$' . $var . ' = ' . $generator->token(['string', 'integer', 'float', 'boolean', 'compare']) . ';';
		IpsumSetVariable::$usedVars[] = $var;
		return $r;
	}

}

class IpsumGetVariable implements IpsumToken {

	public static $usedVars = [];

	public function write(PhpIpsum $generator) {
		if (IpsumSetVariable::$usedVars) {
			return '$' . (IpsumSetVariable::$usedVars[rand(0, count(IpsumSetVariable::$usedVars) - 1)]);
		} else {
			return $generator->token(['string', 'integer', 'float', 'boolean']);
		}
	}

}

class IpsumEcho implements IpsumToken {

	public function write(PhpIpsum $generator) {
		return 'echo ' . $generator->token(['string', 'get_variable', 'concat']) . ';';
	}

}

class IpsumString implements IpsumToken {

	public function write(PhpIpsum $generator) {
		return '"' . PhpIpsum::randomString() . '"';
	}

}

class IpsumCompare implements IpsumToken {

	public function write(PhpIpsum $generator) {

		$comparations = ['==', '<', '>', '!=', '===', '=='];
		$comparation = $comparations[rand(0, count($comparations) - 1)];

		$t1 = $generator->token(['get_variable', 'boolean', 'integer']);
		$t2 = $generator->token(['get_variable', 'boolean', 'integer']);

		return $t1 . ' ' . $comparation . ' ' . $t2;
	}

}

class IpsumIf implements IpsumToken {

	public function write(PhpIpsum $generator) {
		$i = 1;
		while (rand(0, 2) == 1) {
			$i++;
		}

		$linesString = $generator->generateLines($i);

		$lines = explode("\n", $linesString);
		for ($i = 0; $i < count($lines); $i++) {
			$lines[$i] = "  " . $lines[$i];
		}

		return "if (" . $generator->token(['compare', 'compare', 'get_variable', 'boolean']) . ") {\n" .
				implode("\n", $lines) . "\n" .
				"}";
	}

}

class IpsumIfElse extends IpsumIf implements IpsumToken {

	public function write(PhpIpsum $generator) {
		$i = 1;
		while (rand(0, 2) == 1) {
			$i++;
		}

		$linesString = $generator->generateLines($i);

		$lines = explode("\n", $linesString);
		for ($i = 0; $i < count($lines); $i++) {
			$lines[$i] = "  " . $lines[$i];
		}

		return parent::write($generator) . " else {\n" .
				implode("\n", $lines) . "\n" .
				"}";
	}

}

class IpsumFor implements IpsumToken {

	public function write(PhpIpsum $generator) {
		$i = 1;
		while (rand(0, 2) == 1) {
			$i++;
		}

		$linesString = $generator->generateLines($i);

		$lines = explode("\n", $linesString);
		for ($i = 0; $i < count($lines); $i++) {
			$lines[$i] = "  " . $lines[$i];
		}

		return 'for ($i=0; $i<' . $generator->token(['integer']) . '; $i++) {' . "\n" .
				implode("\n", $lines) . "\n" .
				"}";
	}

}
