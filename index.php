<!DOCTYPE html>

<html>
    <head>
        <meta charset="UTF-8">
        <title>PhpIpsum</title>
    </head>
    <body>
		<h1>PhpIpsum</h1>
		<pre><?php
			require_once __DIR__ . '/PhpIpsum.php';
			require_once __DIR__ . '/PhpIpsumTokens.php';

			$phpIpsum = new \Ienze\PhpIpsum\PhpIpsum();

			$php = $phpIpsum->generateBlocks(3);
			
			echo htmlentities("<?php\n\n" . $php . "\n\n?>");
			?></pre>
		<h2>Result:</h2>
		<?php
			eval($php);
		?>
    </body>
</html>
