<?php

/**
 * PHP Ipsum Generator
 *
 * PHP version 5.3+
 *
 * Licensed under The MIT License.
 * Redistribution of these files must retain the above copyright notice.
 *
 * @author    Dominik Gmiterko <d.gmitrko@gmail.com>
 * @copyright Copyright 2015, Dominik Gmiterko
 * @license   http://www.opensource.org/licenses/mit-license.html
 * @link      https://ienze.me/project/php-ipsum
 */

namespace Ienze\PhpIpsum;

class PhpIpsum {

	public static $strings = ['wow', 'amaze', 'excite', 'such drift', 'much dignity', 'very leeder', 'many robe', 'so gif', 'very guitar', 'such fall', 'so trick', 'much feels', 'very sad', 'many happy', 'such pbs', 'very art'];
	private $tokens = [];
	private $baseTokens = [];

	public function __construct() {
		$this->tokens['boolean'] = new IpsumBoolean();
		$this->tokens['integer'] = new IpsumInteger();
		$this->tokens['float'] = new IpsumFloat();
		$this->tokens['echo'] = new IpsumEcho();
		$this->tokens['string'] = new IpsumString();
		$this->tokens['set_variable'] = new IpsumSetVariable();
		$this->tokens['get_variable'] = new IpsumGetVariable();
		$this->tokens['concat'] = new IpsumConcat();
		$this->tokens['if'] = new IpsumIf();
		$this->tokens['ifelse'] = new IpsumIfElse();
		$this->tokens['compare'] = new IpsumCompare();
		$this->tokens['for'] = new IpsumFor();

		$this->baseTokens = ['echo', 'echo', 'set_variable', 'set_variable', 'if', 'ifelse', 'for'];
	}

	public static function randomString() {
		return self::$strings[rand(0, count(self::$strings) - 1)];
	}

	public function token($types = null) {
		if ($types == null || is_array($types)) {
			if ($types == null) {
				$types = $this->baseTokens;
			}

			$type = $types[rand(0, count($types) - 1)];
		} else {
			$type = $types;
		}

		return $this->tokens[$type]->write($this);
	}

	public function generateLine() {
		return $this->token();
	}

	public function generateLines($count = 1, $array = false) {
		$lines = array();

		for ($i = 0; $i < $count; $i++) {
			$lines[] = $this->generateLine();
		}

		if (!$array) {
			return implode("\n", $lines);
		} else {
			return $lines;
		}
	}

	public function generateBlocks($count = 1) {
		$blocks = array();

		for ($i = 0; $i < $count; $i++) {
			$blocks[] = $this->generateLines($this->gauss(2.4, 0.93));
		}

		return implode("\n\n", $blocks);
	}

	/**
	 * Gaussian Distribution
	 * Josh Sherman <josh@gravityblvd.com>
	 *
	 * This is some smart kid stuff. I went ahead and combined the N(0,1) logic
	 * with the N(m,s) logic into this single function. Used to calculate the
	 * number of words in a sentence, the number of sentences in a paragraph
	 * and the distribution of commas in a sentence.
	 *
	 * @access private
	 * @param  double  $mean average value
	 * @param  double  $std_dev stadnard deviation
	 * @return double  calculated distribution
	 */
	private function gauss($mean, $std_dev) {
		$x = mt_rand() / mt_getrandmax();
		$y = mt_rand() / mt_getrandmax();
		$z = sqrt(-2 * log($x)) * cos(2 * pi() * $y);

		return $z * $std_dev + $mean;
	}

}
